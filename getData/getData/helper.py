import re


class notValidString(Exception):
    pass;

class unknownType(Exception):
    pass;

def formatter(typeName, value):
    if not value:
        return 0
    if typeName == 'price':
        value = value[0].strip()
        value = "".join(value.split('.'))
        m = re.match('(\d+)\\xa0Ft', value)
        if not m:
            raise notValidString('"{}" is not a valid price format.'.format(value))
        return int(m.group(1))
    elif typeName == 'infosor':
        value = value.replace(u'\xa0', u' ')
        m = re.match(
            '.*?(\d{4})\/{0,1}(\d{0,2})'
            '.*?(Benzin|Dízel|Benzin\/elektromos|Dízel\/elektromos|LPG\/benzin|CNG\/benzin|LPG\/dízel|CNG\/dízel'
            '|Elektromos|Etanol|Biodízel|Gáz|Hidrogén\/elektromos|Kerozin|Hibrid)'
            '.*?(\d{0,4})\scm.*?\((\d+)\sLE\).*',
            value)
        if not m:
            raise notValidString('"{}" is not a valid infosor format.'.format(value))
        year = int(m.group(1))
        if not m.group(2):
            month = 0
        else:
            month = int(m.group(2))
        fuel = m.group(3)
        engineSize = int(m.group(4))
        horsePower = int(m.group(5))
        return year, month, fuel, engineSize, horsePower
    elif typeName == 'mileage':
        if not value:
            return 0
        else:
            return int("".join([i for i in value if i.isdigit()]))
    else:
        raise unknownType('No such type.')
