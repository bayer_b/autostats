#import scrapy

from scrapy.loader import ItemLoader
from scrapy.selector import Selector
from getData.items import GetdataItem
from scrapy.spiders.init import InitSpider
from scrapy import Request, FormRequest
from getData.helper import formatter

class carSpider(InitSpider):
    """Crawls data from hasznaltauto.hu"""

    name = "cars"
    start_urls = ["https://www.hasznaltauto.hu/kereso"]

    formData = {
        'kereso': 'auto',
        'munkagep': '',
        'q1': '0',
        'q2': '0',
        'tipusjel': '',
        'evjaratmin': '0',
        'evjaratmax': '0',
        'kivitel': '',
        'vetelarmin': '',
        'vetelarmax': '',
        'futottkmmin': '',
        'futottkmmax': '',
        'motor': '0',
        'allapot': '1',
        'hengerurtmin': '',
        'hengerurtmax': '',
        'ajtok_szama': '',
        'ulesek_szama': '',
        'results': '10000',
        'k_ha': '1',
        'k_sza': '1',
        'k_gha': '1',
        'k_bha': '1'
    }


    handle_httpstatus_list = [301, 302]


    def redirectFollow(self, response):
        self.logger.debug("(redirectFollow) start")
        if response.status in [302] and 'Location' in response.headers:
            self.logger.debug("(redirectFollow) Location header: {}"
                              .format(response.headers['Location'].decode("utf-8")))
            yield Request(
                response.urljoin(response.headers['Location'].decode("utf-8")),
                callback=self.parsePage
            )

    def parsePage(self, response):
        self.logger.debug("(parsePage) start")
        s = Selector(response)
        w = s.xpath(
            '//body/div[@id="wrapper"]/div[@id="page"]/div[@id="main"]/div[@id="main_nagyoldal_felcserelve"] \
            /div[@class="feherbox lazy_container"]/div[contains(string(), "talalati")]'
        )
        for l in w:
            item = GetdataItem()
            try:
                item['price'] = formatter('price', l.xpath(
                    './/div[@class="talalati_lista_jobb"]/div[@class="talalati_lista_vetelar"]/div[@class="arsor"] \
                    /strong/text()'
                    ).extract())
                item['bornYear'], item['bornMonth'], item['fuel'], item['engineSize'], item['horsePower'] = \
                    formatter('infosor',
                              l.xpath('.//div[@class="talalati_lista_bal"] \
                              /div[@class="talalati_lista_infosor"]/text()')
                              .extract()[1])
                item['headCont'] = l.xpath(
                    './/div[@class="talalati_lista_bal"]/div[@class="talalati_lista_headcont"] \
                    /div[@class="talalati_lista_head"]/h2/a/text()'
                    ).extract()[0]
                item['mileage'] = formatter('mileage', l.xpath(
                    './/div[@class="talalati_lista_bal"]/div[@class="talalati_lista_infosor"]/abbr/text()'
                    ).extract()[0])
                #self.logger.debug(item)
                yield item
            except Exception as e:
                self.logger.critical('Failed to extract item: "{}".'.format(e))

        nextPage = response.xpath(
                   '//body/div[@id="wrapper"]/div[@id="page"]/div[@id="main"]/div[@id="main_nagyoldal_felcserelve"] \
                   /div[@class="feherbox lazy_container"]/div[@class="noprint"]/div[@class="oldalszamozas"] \
                   /a[@title="Következő oldal"]/@href'
                    ).extract()[0]

        if nextPage is not None:
            yield response.follow(nextPage, callback=self.parsePage)

    def parse(self, response):
        return [
            FormRequest(
                url=self.start_urls[0],
                method='POST',
                formdata=self.formData,
                callback=self.redirectFollow,
                errback=self.requestFailed
            )
        ]

    def requestFailed(self, failure):
        self.logger.error(repr(failure))
        if failure.check(HttpError):
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)
        elif failure.check(DNSLookupError):
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)
        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)



