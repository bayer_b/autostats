# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class GetdataItem(scrapy.Item):
    price = scrapy.Field()
    bornYear = scrapy.Field()
    bornMonth = scrapy.Field()
    fuel = scrapy.Field()
    engineSize = scrapy.Field()
    mileage = scrapy.Field()
    horsePower = scrapy.Field()
    headCont = scrapy.Field()
